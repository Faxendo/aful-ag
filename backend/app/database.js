const config = require("../config/config.json");
const { Sequelize } = require("sequelize");

module.exports = new Sequelize({
    dialect: "mysql",
    database: "afulag",
    host: config.database.hostname,
    username: config.database.username,
    password: config.database.password,
});