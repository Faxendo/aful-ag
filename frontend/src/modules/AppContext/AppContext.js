import React from "react";
import axios from "axios";

const appConfig = {
  apiUrl: "https://api.aful.lakebir.fr/"
};

appConfig.checkToken = () => new Promise((resolve, reject) => {
  const token = localStorage.getItem("token");
  
  if (token === null) {
    reject();
  }

  axios.post(`${appConfig.apiUrl}/user/check`, {
    token
  }).then(res => {
    if (res.data.ok) {
      resolve(token);
    } else {
      reject();
    }
  }).catch(err => {
    reject();
  })
});

export const AppContext = React.createContext(
  appConfig
);