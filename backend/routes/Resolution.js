const express = require("express");
const router = express.Router();
const middleware = require("../app/middleware");

const Resolution = require("../models/Resolution");
const Vote = require("../models/Vote");

router.use(middleware.isAuthenticated);

router.post("/:id/vote", async(req, res) => {
    let resolution = await Resolution.findByPk(req.params.id);
    if (resolution == null) {
        return res.status(404).send({
            error: "Résolution introuvable",
        });
    }

    let existingVote = await Vote.findOne({
        where: {
            ResolutionId: req.params.id,
            UserId: req.user.id,
        },
    });

    if (existingVote !== null) {
        return res.status(403).send({
            error: "Vous avez déjà voté pour cette résolution",
        });
    }

    await Vote.create({
        vote: req.body.vote,
        UserId: req.user.id,
        ResolutionId: req.params.id,
    });

    res.status(200).send({ ok: true });
});

module.exports = router;