const { DataTypes, Model } = require("sequelize");
const database = require("../app/database");

const Resolution = require("./Resolution");

class AG extends Model {}

AG.init({
    date: {
        type: DataTypes.SMALLINT,
        allowNull: false,
    },
    debutVote: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    finVote: {
        type: DataTypes.DATE,
        allowNull: false,
    },
}, {
    sequelize: database,
    modelName: "AG",
});

AG.hasMany(Resolution);

module.exports = AG;