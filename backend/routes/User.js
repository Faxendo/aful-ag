const crypto = require("crypto");
const express = require("express");
const router = express.Router();
const config = require("../config/config.json");

const jwt = require("jsonwebtoken");

const User = require("../models/User");

router.post("/login", async(req, res) => {
    try {
        hashedPassword = crypto
            .createHash("sha256")
            .update(req.body.password)
            .digest("base64");
        let user = await User.findOne({
            where: {
                identifiant: req.body.username,
                motDePasse: hashedPassword,
            },
        });

        if (user) {
            const token = jwt.sign({
                    id: user.id,
                    identifiant: user.identifiant,
                    nom: user.nom,
                    rang: user.rang,
                },
                config.app.secret, {
                    expiresIn: "1 day",
                }
            );
            res.send({ token });
        } else {
            res.status(404).send({ error: "Cet utilisateur n'existe pas" });
        }
    } catch (err) {
        if (err) {
            console.error(err);
            res.status(500).send(err);
        }
    }
});

router.post("/check", async(req, res) => {
    try {
        jwt.verify(req.body.token, config.app.secret);
        res.send({
            ok: true,
        });
    } catch (err) {
        res.send({
            ok: false,
            message: err,
        });
    }
});

router.post("/", async(req, res) => {
    try {
        let user = await User.create(req.body);
        console.log(user);
        res.send(user);
    } catch (err) {
        console.error(err);
        res.status(500).send(err);
    }
});

module.exports = router;