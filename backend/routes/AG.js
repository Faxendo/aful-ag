const express = require("express");
const router = express.Router();
const middleware = require("../app/middleware");

const moment = require("moment");

const AG = require("../models/AG");
const Resolution = require("../models/Resolution");
const Vote = require("../models/Vote");

router.use(middleware.isAuthenticated);

router.get("/", async(req, res) => {
    try {
        let ags = await AG.findAll({ include: Resolution });
        res.send(ags);
    } catch (err) {
        res.status(500).send(err);
    }
});

router.get("/:id", async(req, res) => {
    try {
        let ag = await AG.findOne({
            where: {
                id: req.params.id,
            },
        });

        if (ag === null) {
            return res.send(404);
        }

        let now = moment();
        let debutVote = moment(ag.debutVote);
        let finVote = moment(ag.finVote);

        ag.votesOuverts = now.isAfter(debutVote);
        ag.resultatsVisibles = now.isAfter(finVote);

        let resolutions = await Resolution.findAll({
            where: {
                AGId: ag.id,
            },
            include: Vote,
        });

        // let minifiedResolutions = resolutions.map((r) => {
        //     let rTmp = r;
        //     let findMyVote = rTmp.Votes.find((v) => v.UserId === req.user.id);
        //     if (findMyVote) rTmp.monVote = findMyVote.vote;

        //     rTmp.Votes = null;

        //     return rTmp;
        // });

        let minifiedResolutions = [];

        for (let r of resolutions) {
            let rTmp = {
                id: r.id,
                ordre: r.ordre,
                titre: r.titre,
            };

            let findMyVote = r.Votes.find((v) => v.UserId === req.user.id);
            if (findMyVote) rTmp.monVote = findMyVote.vote;

            if (ag.resultatsVisibles) {
                let pour = r.Votes.filter((v) => v.vote === "pour").length;
                let abstention = r.Votes.filter((v) => v.vote === "abstention").length;
                let contre = r.Votes.filter((v) => v.vote === "contre").length;

                let total = 0;
                total += pour + abstention + contre;

                let pourcentPour, pourcentAbstention, pourcentContre;
                pourcentPour = (pour / total) * 100;
                pourcentAbstention = (abstention / total) * 100;
                pourcentContre = (contre / total) * 100;

                rTmp.votes = {
                    pour: {
                        nombre: pour,
                        pourcent: pourcentPour,
                    },
                    abstention: {
                        nombre: abstention,
                        pourcent: pourcentAbstention,
                    },
                    contre: {
                        nombre: contre,
                        pourcent: pourcentContre,
                    },
                };
            }

            minifiedResolutions.push(rTmp);
        }

        res.send({
            ag: {
                id: ag.id,
                date: ag.date,
                debutVote: ag.debutVote,
                finVote: ag.finVote,
                votesOuverts: ag.votesOuverts,
                resultatsVisibles: ag.resultatsVisibles,
                votes: ag.votes,
            },
            resolutions: minifiedResolutions,
        });
    } catch (err) {
        console.error(err);
        res.status(500).send(err);
    }
});

module.exports = router;