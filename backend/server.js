const config = require("./config/config.json");

const express = require("express");
const bp = require("body-parser");

const database = require("./app/database");

const app = express();

const UserRoutes = require("./routes/User");
const AGRoutes = require("./routes/AG");
const ResolutionRoutes = require("./routes/Resolution");
const VoteRoutes = require("./routes/Vote");
const FileRoutes = require("./routes/File");

app.use(bp.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, X-JWT-Token"
    );
    next();
});

app.options("*", (req, res) => {
    // allowed XHR methods
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, X-JWT-Token"
    );
    res.header(
        "Access-Control-Allow-Methods",
        "GET, PATCH, PUT, POST, DELETE, OPTIONS"
    );
    res.send(200);
});

app.use("/user", UserRoutes);
app.use("/ag", AGRoutes);
app.use("/resolution", ResolutionRoutes);
app.use("/vote", VoteRoutes);
app.use("/file", FileRoutes);

app.get("/", (req, res) => {
    res.json({
        status: true,
    });
});

app.listen(config.server.port, () => {
    console.log(`Listening on ${config.server.port}`);
    database.sync({
        force: false,
    });
});