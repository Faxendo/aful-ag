import "./App.css";

import React from "react";

import {BrowserRouter, Switch, Route} from "react-router-dom";

import Login from "./modules/Login/Login";
import ListAG from "./modules/ListAG/ListAG";
import ViewAG from "./modules/ViewAG/ViewAG";

import "bootstrap/dist/css/bootstrap.min.css";

import {AppContext} from "./modules/AppContext/AppContext"

export default class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route exact path="/liste" component={ListAG} />
          <Route path="/ag/:id" component={ViewAG} />
        </Switch>
      </BrowserRouter>
    );
  }
}