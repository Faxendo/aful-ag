const { DataTypes, Model } = require("sequelize");
const database = require("../app/database");

const Vote = require("./Vote");

class User extends Model {}

User.init({
    nom: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    identifiant: {
        type: DataTypes.SMALLINT,
        allowNull: false,
    },
    motDePasse: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    rang: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            isIn: [
                ["membre", "comite"]
            ],
        },
    },
}, {
    sequelize: database,
    modelName: "User",
});

User.hasMany(Vote);

module.exports = User;