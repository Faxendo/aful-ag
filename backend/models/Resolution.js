const { DataTypes, Model } = require("sequelize");
const database = require("../app/database");

const Vote = require("./Vote");

class Resolution extends Model {}

Resolution.init({
    ordre: {
        type: DataTypes.TINYINT,
        allowNull: false,
    },
    titre: {
        type: DataTypes.STRING,
        allowNull: false,
    },
}, {
    sequelize: database,
    modelName: "Resolution",
});

Resolution.hasMany(Vote);

module.exports = Resolution;