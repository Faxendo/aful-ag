const { DataTypes, Model } = require("sequelize");
const database = require("../app/database");

class Vote extends Model {}

Vote.init({
    vote: {
        type: DataTypes.STRING,
        allowNull: false,
    },
}, {
    sequelize: database,
    modelName: "Vote",
});

module.exports = Vote;