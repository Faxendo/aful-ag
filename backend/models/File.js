const { DataTypes, Model } = require("sequelize");
const database = require("../app/database");

const Resolution = require("./Resolution");

class File extends Model {}

File.init({
    titre: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    lien: {
        type: DataTypes.STRING,
        allowNull: false,
    },
}, {
    sequelize: database,
    modelName: "File",
});

File.belongsTo(Resolution);

module.exports = File;