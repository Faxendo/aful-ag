class Controller {
    static response(status, body) {
        return {
            status,
            body,
        };
    }
}

module.exports = Controller;