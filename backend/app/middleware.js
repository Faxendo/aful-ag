const config = require("../config/config.json");
const jwt = require("jsonwebtoken");

module.exports = {
    isAuthenticated: (req, res, next) => {
        try {
            req.user = jwt.verify(req.header("X-JWT-Token"), config.app.secret);
            next();
        } catch (err) {
            res.status(403).send({ error: "Forbidden" });
        }
    },

    comiteOnly: (req, res, next) => {},
};