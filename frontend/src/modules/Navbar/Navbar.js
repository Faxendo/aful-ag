import "./Navbar.css";

import React from "react";

import {Link} from "react-router-dom";

import {Navbar, Nav, Button} from "react-bootstrap";

export default class NavMenu extends React.Component {
  render() {
    return (
      <Navbar>
        <Navbar.Brand href="#home">AG AFUL CT2</Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse id="navbar">
          <Nav className="ml-auto">
            {/* <Navbar.Text>Bonjour %USER%</Navbar.Text> */}
            <Button variant="danger" as={Link} to={`/`}>Quitter</Button>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}