import "./ListAG.css";

import React from "react";
import axios from "axios";
import {Link} from "react-router-dom";
import {AppContext} from "../AppContext/AppContext"

import NavMenu from "../Navbar/Navbar"

import {Container, Row, Col, Card, Button} from "react-bootstrap";

export default class ListAG extends React.Component {
  static contextType = AppContext;
  token = null;

  state = {
    ags: []
  }

  async componentDidMount(){
    let token;
    try {
      token = await this.context.checkToken();
      //axios.defaults.headers.common['X-JWT-Token'] = token;
    } catch (err) {
      this.props.history.push("/");
    }

    axios.get(`${this.context.apiUrl}/ag/`, {
      headers: {
        "X-JWT-Token": token
      }
    }).then(res => {
      this.setState({ags: res.data});
    });
  }

  render() {
    return ( <div>
      <NavMenu />
      <Container className="text-center">
      <Row className="justify-content-center mt-5">
        <Col xs={12} md={8}>
          <Card>
            <h4 className="card-header">Intervention du Président</h4>
            <Card.Body>
              <p>L’impact de la Covid-19 sur la population, le monde du travail, les déplacements et le fonctionnement des institutions ont conduit à un désordre d'une portée et d'une proportion sans précédent. Toutefois, cette crise n’a pas empêché le comité syndical de mener à bien sa mission de gestion de la Commanderie 2. Tous les travaux prévus en 2020 ont été exécutés et les difficultés de fonctionnement ou incidents techniques ont pu être résolus grâce à la ténacité des syndics. En revanche, les mesures sanitaires nous empêchent de tenir notre Assemblée générale dans les conditions habituelles.</p>
              <p>Nous sommes donc conduits à organiser une AG virtuelle avec un ordre du jour réduit se limitant à la gestion financière et à l’élection des membres du Comité syndical. Toutefois, ceux qui le souhaitent peuvent voter par correspondance. Un formulaire sera mis à leur disposition. Avant de voter, veuillez consulter la note explicative jointe à la convocation de l’AG 2021. Elle vous donne toutes les explications sur la situation financière de l’AFUL 2 et sur le renouvellement du Comité syndical.</p>
              <br />
              <p>L’année 2020 a surtout été marquée par les importants travaux de voirie, réalisés dans de très bonnes conditions, et par le départ de M. Boutin, notre jardinier depuis 6 ans, remplacé par M. Kyei. Ce dernier donne entière satisfaction. Nous avons eu quelques soucis avec l’assainissement à cause des lingettes jetées dans les toilettes et effectué des travaux de réfection ou d’entretien sur les tennis et l’éclairage public. Les espaces verts ont fait l’objet d’une attention particulière comme tous les ans.</p>
              <p>Neuf familles ont été malheureusement touchées par un deuil et nous avons eu connaissance de 4 naissances. Douze maisons ont changé de propriétaires.</p>
              <br />
              <p>Vous serez donc invités à vous prononcer sur le bilan financier 2020 qui, comme le prouve les tableaux qui vous sont soumis, n’appelle pas de remarque particulière même si les travaux de voirie ont grevé le compte de provisions. Mais ce déficit avait été « programmé » après la décision de l’AG 2020 de refaire en totalité l’enrobé de plusieurs rues sans augmentation de la cotisation. (1ère résolution).</p>
              <br />
              <p>Il faudra aussi autoriser l’affectation du résultat de l’exercice 2020 au compte de provisions (2ème résolution).</p>
              <br />
              <p>Vous devrez ensuite vous prononcer sur le montant de la cotisation 2021 qui restera toujours au même niveau que celle de 2007. (3ème et 4ème résolutions).</p>
              <br />
              <p>La cooptation de M. Gaucher et la réélection de Mme Pujol et MM. Galvao et Mercier comme syndics font l’objet de la 7ème résolution de l’ordre du jour, sachant que les résolutions 5 et 6 sont reportées.</p>
              <br />
              <p>Merci pour votre participation.</p>
              <p>Le Président.</p>
            </Card.Body>
          </Card>
        </Col>
        <Col xs={12} md={4}>
          <Row className="justify-content-around">
            { this.state.ags.map(ag => 
              <Col key={ag.id} xs={12}>
                <Card>
                  <h4 className="card-header">Assemblée Générale {ag.date}</h4>
                  <Card.Body>
                    <p>Votes ouverts du {Intl.DateTimeFormat("fr").format(Date.parse(ag.debutVote))} au {Intl.DateTimeFormat("fr").format(Date.parse(ag.finVote))}</p>
                    <p>{ag.Resolutions.length} résolutions présentées</p>
                  </Card.Body>
                  <Card.Footer className="d-flex justify-content-around">
                    <Button as={Link} to={`/ag/${ag.id}`} variant="primary">Accéder à l'Assemblée Générale</Button>
                  </Card.Footer>
                </Card>
              </Col>
            )}
          </Row>
        </Col>
      </Row>
    </Container>
    </div>);
  }
}