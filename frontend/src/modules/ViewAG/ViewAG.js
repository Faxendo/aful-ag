import "./ViewAG.css";

import React from "react";
import axios from "axios";
import {AppContext} from "../AppContext/AppContext"

import NavMenu from "../Navbar/Navbar"

import {Container, Row, Col, Card, Button, Badge, ProgressBar} from "react-bootstrap";

export default class ListAG extends React.Component {
  static contextType = AppContext;

  state = {
    ag: null
  }

  voirQuestions(r){
    let ag = this.state.ag;
    let resolution = ag.resolutions.find(res => res.ordre === r.ordre);
    resolution.voirQuestions = true;
    this.setState({ag});
  }

  cacherQuestions(r){
    let ag = this.state.ag;
    let resolution = ag.resolutions.find(res => res.ordre === r.ordre);
    resolution.voirQuestions = false;
    this.setState({ag});
  }

  handleVote = (resolution, vote) => {
    if(window.confirm(`Résolution ${resolution.ordre} : vous vous apprêtez à voter ${vote.toUpperCase()}. Continuer ?`)) {
      axios.post(`${this.context.apiUrl}/resolution/${resolution.id}/vote`, {
        vote
      }).then(res => {
        if (res.data.ok) {
          this.loadAg();
        }
      }).catch(err => {
        alert(err.response.data.error);
      })
    }
  }

  loadAg(){
    axios.get(`${this.context.apiUrl}/ag/${this.props.match.params.id}`).then(res => {
      let ag = res.data.ag;
      ag.resolutions = res.data.resolutions;

      this.setState({ag});
    });
  }

  async componentDidMount(){
    let token;
    try {
      token = await this.context.checkToken();
      axios.defaults.headers.common['X-JWT-Token'] = token;
    } catch (err) {
      this.props.history.push("/");
    }

    this.loadAg();
  }

  render() {
    return ( <div>
    <NavMenu />
    { this.state.ag !== null && <Container className="text-center">
      <Row className="justify-content-start mt-5">
        <h1>Assemblée Générale {this.state.ag.date}</h1>
      </Row>
      <Row className="justify-content-center">
        <Col xs={12}>
            { this.state.ag.resolutions.map(r => 
              <div key={r.id}>
              <Row className="mt-3">
                <Col xs={12}>
                  <Card>
                    <h4 className="card-header">Résolution {r.ordre} : <small>{r.titre}</small></h4>
                    <Card.Body>
                      <Row className="justify-content-start">
                        {/* <Col xs={12}>
                          Pièces jointes : <a href="">Fichier de test</a>
                        </Col> */}
                        {this.state.ag.resultatsVisibles && <Col xs={12}>
                          <Row className="justify-content-around mt-3">
                            <Col xs={12} md={4}>
                              <Badge variant="success">Pour : {r.votes.pour.nombre}</Badge>
                            </Col>
                            <Col xs={12} md={4}>
                              <Badge variant="secondary">Abstention : {r.votes.abstention.nombre}</Badge>
                            </Col>
                            <Col xs={12} md={4}>
                              <Badge variant="danger">Contre : {r.votes.contre.nombre}</Badge>
                            </Col>
                          </Row>
                          <Row className="justify-content-center mt-2">
                            <Col xs={12}>
                              <ProgressBar>
                                <ProgressBar variant="success" now={r.votes.pour.pourcent} key={1}></ProgressBar>
                                <ProgressBar variant="secondary" now={r.votes.abstention.pourcent} key={2}></ProgressBar>
                                <ProgressBar variant="danger" now={r.votes.contre.pourcent} key={3}></ProgressBar>
                              </ProgressBar>
                            </Col>
                          </Row>
                        </Col>}
                        {this.state.ag.votesOuverts && !this.state.ag.resultatsVisibles && r.monVote !== null && <Col xs={12}>
                          <Row className="justify-content-center mt-3">
                            {r.monVote === "pour" && <Col xs={12} md={4}>
                              <Badge variant="success">Vous avez voté : Pour</Badge>
                            </Col>}
                            {r.monVote === "abstention" && <Col xs={12} md={4}>
                              <Badge variant="secondary">Vous avez voté : Abstention</Badge>
                            </Col>}
                            {r.monVote === "contre" && <Col xs={12} md={4}>
                              <Badge variant="danger">Vous avez voté : Contre</Badge>
                            </Col>}
                          </Row>
                        </Col>}
                        {!this.state.ag.votesOuverts && !this.state.ag.resultatsVisibles && <Col xs={12}>
                          <Row className="justify-content-around mt-3">
                            <Col xs={12}>
                              Les votes seront ouverts le {Intl.DateTimeFormat("fr").format(Date.parse(this.state.ag.debutVote))}
                            </Col>
                          </Row>
                        </Col>}
                        {this.state.ag.votesOuverts && !this.state.ag.resultatsVisibles && r.monVote == null && <Col xs={12}>
                          <Row className="justify-content-center mt-3">
                            <Col xs={12} md={4}>
                              <Button variant="success" onClick={() => this.handleVote(r, "pour")}>Pour</Button>
                            </Col>
                            <Col xs={12} md={4}>
                              <Button variant="secondary" onClick={() => this.handleVote(r, "abstention")}>Abstention</Button>
                            </Col>
                            <Col xs={12} md={4}>
                              <Button variant="danger" onClick={() => this.handleVote(r, "contre")}>Contre</Button>
                            </Col>
                          </Row>
                        </Col>
                        }
                      </Row>
                    </Card.Body>
                    {/* <Card.Footer>
                      <Button variant="info" onClick={() => this.voirQuestions(r)}>{r.questions.length} question(s) posée(s)</Button>
                    </Card.Footer> */}
                  </Card>
                </Col>
              </Row>
              {/* <Modal size="lg" onHide={() => this.cacherQuestions(r)} show={r.voirQuestions}>
                <Modal.Header>
                  <Modal.Title>Questions &amp; Réponses pour la résolution {r.ordre}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <Row>
                    <Col xs={12} className="text-center">
                      <b>{r.titre}</b>
                    </Col>
                  </Row>
                  <Row className="justify-content-center mt-2">
                    <Col xs={12} md={9}>
                      <Form.Control placeholder="Poser une question"></Form.Control>
                    </Col>
                    <Col xs={12} md={3} className="text-center">
                      <Button variant="primary">Envoyer</Button>
                    </Col>
                  </Row>
                  <hr />
                  {r.questions.map(q => 
                  <Row className="mt-2">
                    <Col xs={12}>
                      <Card>
                        <Card.Header>
                          <b>{q.auteur} demande : {q.question}</b>
                        </Card.Header>
                        <Card.Body>
                          {q.reponses.map(r => 
                            <Row>
                              <Col xs={12}>
                                <b>{r.auteur} a répondu : </b> {r.reponse}
                              </Col>
                            </Row>
                          )}
                        </Card.Body>
                        <Card.Footer>
                          <Row className="justify-content-center">
                            <Col xs={12} md={9}>
                              <Form.Control placeholder="Votre réponse..."></Form.Control>
                            </Col>
                            <Col xs={12} md={3} className="text-center">
                              <Button variant="primary">Répondre</Button>
                            </Col>
                          </Row>
                        </Card.Footer>
                      </Card>
                    </Col>
                  </Row>)}
                </Modal.Body>
              </Modal> */}
              </div>
            )}
        </Col>
      </Row>
    </Container>}
    </div>);
  }
}