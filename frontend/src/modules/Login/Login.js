import "./Login.css";

import React from "react";
import {AppContext} from "../AppContext/AppContext"

import axios from "axios";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

export default class Login extends React.Component {
  static contextType = AppContext;

  state = {
    identifiant: "",
    motDePasse: ""
  }

  handleUsernameChange = event => {
    this.setState({
      identifiant: event.target.value
    });
  }

  handlePasswordChange = event => {
    this.setState({
      motDePasse: event.target.value
    });
  }

  login = event => {
    event.preventDefault();
    axios.post(`${this.context.apiUrl}/user/login`, {
      username: this.state.identifiant,
      password: this.state.motDePasse
    }).then(res => {
      localStorage.setItem("token", res.data.token);
      this.props.history.push("/liste");
    }).catch(err => {
      console.error(err);
      alert(err.response.data.error);
    });
  }

  render(){
    return (
      <Container className="text-center">
        <Row className="justify-content-center mt-5">
          <Col xs={12} md={8}>
            <Card>
              <Card.Body>
                <h1>AFUL Commanderie des Templiers 2</h1>
                <h2>Plateforme de vote - Assemblée Générale</h2>
                <Form onSubmit={this.login}>
                  <Row className="justify-content-center">
                    <Col xs={12} md={6}>
                      <Form.Group>
                        <Form.Label htmlFor="identifiant" className="visually-hidden">Identifiant</Form.Label>
                        <Form.Control id="identifiant" type="text" placeholder="Votre identifiant" onChange={this.handleUsernameChange} />
                      </Form.Group>
                    </Col>
                    <Col xs={12} md={6}>
                      <Form.Group>
                        <Form.Label htmlFor="motDePasse" className="visually-hidden">Mot de passe</Form.Label>
                        <Form.Control id="motDePasse" type="password" placeholder="Votre mot de passe" onChange={this.handlePasswordChange} />
                      </Form.Group>
                    </Col>
                  </Row>
                  
                  <Button type="submit" variant="primary">Connexion</Button>
                </Form>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}